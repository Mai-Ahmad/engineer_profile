<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BlogController extends Controller
{
    public function index(){
        $blogs=Blog::orderBy('created_at','desc')->get();
        return view ('admin.blogs.index',['blogs'=>$blogs]);
    }

    public function create(){
        return view('admin.blogs.create');
    }
    public function store(Request $request){
        if($request->file != ''){
            $path = 'images/blogs/';


            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);


        }
        $blog=Blog::create([
            'image'      =>$filename,
            'ar'=>['title'=>$request->title_ar
                ,'content' => $request->content_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'content' => $request->content_en
            ]
        ]);


        return redirect(route('admin_panel.blogs.index'))->with(['message'=>'تمت الإضافة بنجاح']);
    }
    public function edit(Request $request,$id){
        $blog=Blog::find($id);
        return view('admin.blogs.edit',['blogs'=>$blog]);
    }

    public function update(Request $request,$id){
        $blog=Blog::find($id);
        if($request->file != ''){
            (File::exists($blog->image)) ? File::delete($blog->image) : Null;
            $path = 'images/blogs/';

            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);

            $blog->update([
                'image'      =>$filename
            ]);
        }

        $blog->update([
            'ar'=>['title'=>$request->title_ar
                ,'content' => $request->content_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'content' => $request->content_en
            ]
        ]);

        return redirect(route('admin_panel.blogs.index'))->with(['message'=>'تم التعديل بنجاح']);
    }
    public function destroy($id){
        $blog=Blog::find($id);
        (File::exists($blog->image)) ? File::delete($blog->image) : Null;
        $blog->delete();
        return redirect()->back()->with(['message'=>'تم الحذف بنجاح']);
    }

}
