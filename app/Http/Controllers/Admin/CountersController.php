<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Counters;
use Illuminate\Http\Request;

class CountersController extends Controller
{
    public function index()
    {
        $counters=Counters::get();
        return view ('admin.counters.index',['counters'=>$counters]);
    }

    public function edit(Request $request,$id){
        $counter=Counters::find($id);
        return view('admin.counters.edit',['counter'=>$counter]);
    }

    public function update(Request $request,$id){
        $counter=Counters::find($id);
        $counter->update([
            'ar'=>['name'=>$request->name_ar],
            'en'=>['name'=>$request->name_en],
           'counter'  =>$request->counter
        ]);
        return redirect(route('admin_panel.counters.index'))->with(['message'=>'تم التعديل بنجاح']);
    }
}
