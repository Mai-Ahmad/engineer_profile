<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function edit(Request $request,$id){
        $media=Media::first();
        return view('admin.media',['media'=>$media]);
    }


    public function update(Request $request,$id){
        $media=Media::first();
        $media->update([
            'facebook'       =>$request->facebook,
            'linkedin'       =>$request->linkedin,
            'twitter'        =>$request->twitter,
            'instagram'      =>$request->instagram,
            'youtube'        =>$request->youtube,
            'snapchat'       =>$request->snapchat,
            'whatsapp'       =>$request->whatsapp
        ]);

        return redirect()->back()->with(['message'=>'تم التعديل بنجاح']);
    }
}
