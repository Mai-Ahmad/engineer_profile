<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class MetaController extends Controller
{
    public function index(){
        $meta=Meta::first();
        return view('admin.Meta.index',compact('meta'));
    }

    
    



    public function update(Request $request,$id){
        $meta=Meta::first();

        if($request->image != '' ){
            (File::exists($meta->header_logo)) ? File::delete($meta->header_logo) : Null;
            $path = 'images/meta/';

            $image = $request->image;
            $imageExtension = $image->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $image->move($path, $filename);

            $meta->update([
                'image'      =>$filename
            ]);

        }  
        $meta->update([

            'ar'=>['title'=>$request->title_ar
            ,'description' => $request->description_ar
            ,'keywords' => $request->keywords_ar
        ],
        'en'=>['title'=>$request->title_en
            ,'description' => $request->description_en
            ,'keywords' => $request->keywords_en
        ]
          
        ]);

        return redirect()->back();
     }

}
