<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Metablog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MetablogController extends Controller
{

        public function index(){
            $blog_meta=Metablog::first();
            return view('admin.Meta.blog_meta.index',compact('blog_meta'));
        }
    
         
    public function update(Request $request,$id){
        $blog_meta=Metablog::first();

        if($request->image != '' ){
            (File::exists($blog_meta->header_logo)) ? File::delete($blog_meta->header_logo) : Null;
            $path = 'images/meta/';

            $image = $request->image;
            $imageExtension = $image->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $image->move($path, $filename);

            $blog_meta->update([
                'image'      =>$filename
            ]);

        }  
        $blog_meta->update([

            'ar'=>['title'=>$request->title_ar
            ,'description' => $request->description_ar
            ,'keywords' => $request->keywords_ar
        ],
        'en'=>['title'=>$request->title_en
            ,'description' => $request->description_en
            ,'keywords' => $request->keywords_en
        ]
          
        ]);

        return redirect()->back();
     }

}


