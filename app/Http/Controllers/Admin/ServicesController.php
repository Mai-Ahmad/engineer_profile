<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Services;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ServicesController extends Controller
{
    public function index(){
        $services=Services::orderBy('created_at','desc')->get();
        return view ('admin.services.index',['services'=>$services]);
    }

    public function create(){
        return view('admin.services.create');
    }
    public function store(Request $request){
        if($request->file != ''){
            $path = 'images/services/';


            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);


        }
        $service=Services::create([
            'icon'      =>$filename,
            'ar'=>['title'=>$request->title_ar
                ,'content' => $request->content_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'content' => $request->content_en
            ]
        ]);


        return redirect(route('admin_panel.services.index'))->with(['message'=>'تمت الإضافة بنجاح']);;
    }
    public function edit(Request $request,$id){
        $service=Services::find($id);
        return view('admin.services.edit',['service'=>$service]);
    }

    public function update(Request $request,$id){
        $service=Services::find($id);
        if($request->file != ''){
            (File::exists($service->icon)) ? File::delete($service->icon) : Null;
            $path = 'images/services/';

            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);

            $service->update([
                'icon'      =>$filename
            ]);
        }

        $service->update([
            'ar'=>['title'=>$request->title_ar
                ,'content' => $request->content_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'content' => $request->content_en
            ]
        ]);

        return redirect(route('admin_panel.services.index'))->with(['message'=>'تم التعديل بنجاح']);
    }
    public function destroy($id){
        $service=Services::find($id);
        (File::exists($service->icon)) ? File::delete($service->icon) : Null;
        $service->delete();
        return redirect()->back()->with(['message'=>'تم الحذف بنجاح']);
    }

}
