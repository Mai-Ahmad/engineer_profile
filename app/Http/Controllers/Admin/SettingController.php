<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Type;
use App\Models\YourProject;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function editAbout(){
        $about=Setting::where('name','about')->first();
        return view('admin.about',['about'=>$about]);
    }

    public function updateAbout(Request $request){
        $about=Setting::where('name','about')->first();
        $about->update([
            'ar'  => [
                'title'        => $request->title_ar,
                'content'      => $request->content_ar,
            ],
            'en'  => [
                'title'        => $request->title_en,
                'content'      => $request->content_en,
            ]
        ]);
        return redirect()->back()->with(['message'=>'تم التعديل بنجاح']);
    }

    public function editWe(){
        $we=Setting::where('name','who_are_we')->first();
        return view('admin.who_are_we',['we'=>$we]);
    }

    public function updateWe(Request $request){
        $we=Setting::where('name','who_are_we')->first();
        $we->update([
            'ar'  => [
                'title'        => $request->title_ar,
                'content'      => $request->content_ar,
            ],
            'en'  => [
                'title'        => $request->title_en,
                'content'      => $request->content_en,
            ]
        ]);
        return redirect()->back()->with(['message'=>'تم التعديل بنجاح']);
    }
}
