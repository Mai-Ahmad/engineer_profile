<?php

namespace App\Http\Controllers\Admin;

use App\Models\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
   public function index(){
       Type::create([
           'ar'=>['name'=>'تطبيقات'],
           'en'=>['name'=>'application'],
       ]);
       Type::create([
           'ar'=>['name'=>'مواقع'],
           'en'=>['name'=>'website'],
       ]);
   }
}
