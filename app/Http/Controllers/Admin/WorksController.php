<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Type;
use App\Models\Works;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class WorksController extends Controller
{
    public function index(){
        $works=Works::orderBy('created_at','desc')->get();
        return view ('admin.works.index',['works'=>$works]);
    }

    public function show($id){
        $work=Works::find($id);
        return view('admin.works.item',['work'=>$work]);
    }
    public function create(){
        $types=Type::get();
        return view('admin.works.create',['types'=>$types]);
    }
    public function store(Request $request){
        if($request->file != ''){
            $path = 'images/works/';

            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);

        }
        $works=Works::create([
            'image'      =>$filename,
            'type_id'    =>$request->type_id,
            'link'       =>$request->link,
            'ar'=>['title'=>$request->title_ar
                ,'content' => $request->content_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'content' => $request->content_en
            ]
        ]);


        return redirect(route('admin_panel.works.index'))->with(['message'=>'تمت الإضافة بنجاح']);
    }
    public function edit(Request $request,$id){
        $work=Works::find($id);
        $types=Type::get();
        return view('admin.works.edit',['work'=>$work,'types'=>$types]);
    }

    public function update(Request $request,$id){
        $work=Works::find($id);
        if($request->file != ''){
            (File::exists($work->image)) ? File::delete($work->image) : Null;
            $path = 'images/works/';

            $file = $request->file;
            $filename = $file->getClientOriginalName();
            $filename = $path.$filename;
            $file->move($path, $filename);

            $work->update([
                'image'      =>$filename
            ]);
        }

        $work->update([
            'link'=>$request->link,
            'type_id'=>$request->type_id,
            'ar'=>['title'=>$request->title_ar
                ,'content' => $request->content_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'content' => $request->content_en
            ]
        ]);

        return redirect(route('admin_panel.works.index'))->with(['message'=>'تم التعديل بنجاح']);
    }
    public function destroy($id){
        $work=Works::find($id);
        (File::exists($work->image)) ? File::delete($work->image) : Null;
        $work->delete();
        return redirect()->back()->with(['message'=>'تم الحذف بنجاح']);
    }
}
