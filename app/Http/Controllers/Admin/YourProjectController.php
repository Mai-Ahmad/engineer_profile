<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\YourProject;
use Illuminate\Http\Request;

class YourProjectController extends Controller
{
    public function index(){

        $items=YourProject::get();
        return view ('admin.your_project.index',['items'=>$items]);
    }

    public function edit(Request $request,$id){
        $item=YourProject::find($id);
        return view('admin.your_project.edit',['item'=>$item]);
    }

    public function update(Request $request,$id){
        $item=YourProject::find($id);
        $item->update([
            'ar'=>['content' => $request->content_ar],
            'en'=>['content' => $request->content_en]
        ]);
        return redirect(route('admin_panel.yourProject.index'))->with(['message'=>'تم التعديل بنجاح']);
    }

}
