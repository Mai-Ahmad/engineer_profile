<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Media;
use App\Models\Metablog;
use \Illuminate\Support\Str;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(){

        $blogs=Blog::orderBy('created_at','desc')->paginate(6);
        $media=Media::first();
        $blog_meta=Metablog::first();
        
        return view ('blogs.index',['blogs'=>$blogs,'media'=>$media,'blog_meta'=>$blog_meta]);
    }
    public function show($id){
        $blog=Blog::find($id);
        $media=Media::first();
        $blog_meta=Metablog::first();
        $last_blogs=$blogs=Blog::orderBy('created_at','desc')->take(3)->get();
        return view('blogs.item',['blog'=>$blog,'media'=>$media,'last_blogs'=>$last_blogs, 'blog_meta'=>$blog_meta]);
    }
}
