<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{

    public function index()
    {
        $contact = Contact::orderBy('created_at', 'desc')->get();
        return view('admin.contact.index', ['contact' => $contact]);
    }

    public function show($id)
    {
        $contact = Contact::find($id);
        return view('admin.contact.show', ['contact' => $contact]);
    }

    public function store(Request $request)
    {
        $lang = Session::get('locale');
        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
        ];

        Mail::to('maiahmad0993@gmail.com')->send(new ContactMail($details));

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        $contact->save();
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return redirect()->back()->with(['message' => 'تم الحذف بنجاح']);
    }
}


