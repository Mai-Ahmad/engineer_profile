<?php

namespace App\Http\Controllers;

use App\Models\Counters;
use App\Models\Media;
use App\Models\Services;
use App\Models\Setting;
use App\Models\Type;
use App\Models\Meta;
use App\Models\Works;
use App\Models\YourProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;




use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\JsonLd;
use Artesaos\SEOTools\Facades\TwitterCard;

class HomeController extends Controller
{
    public function index(){
        $about=Setting::where('name','about')->first();
        $who_are_we=Setting::where('name','who_are_we')->first();
        $services=Services::get();
        $step1=YourProject::find(1);
        $step2=YourProject::find(2);
        $step3=YourProject::find(3);
        $step4=YourProject::find(4);
        $works=Works::get();
        $works_app=Works::where('type_id',1)->orderBy('created_at','desc')->get();
        $works_web=Works::where('type_id',2)->orderBy('created_at','desc')->get();
        $media=Media::first();
        $counter1=Counters::find(1);
        $counter2=Counters::find(2);
        $counter3=Counters::find(3);
        $counter4=Counters::find(4);
        $meta=Meta::find(1);
        $type1=Type::find(1);
        $type2=Type::find(2);
        return view('home',compact('about','who_are_we','services','step1','step2','step3','step4','works','media','counter1','counter2','counter3','counter4','type1','type2','meta'));
    }

    public function home(){
        $lang = App::getLocale();
        if($lang){
            return redirect('/'.$lang);
        }else{
            Session::put('applocale', 'en');
            return redirect('/en');
        }
        
    }
}
