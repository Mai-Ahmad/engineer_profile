<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Works extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['type_id','image','link'];

    public $translatedAttributes = ['title','content'];

    public function type(){
        return $this->belongsTo(Type::class);
    }
}
