<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYourProjectTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('your_project_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('your_project_id')->constrained('your_projects')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->string('title');
            $table->text('content');
            $table->unique(['your_project_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('your_project_translations');
    }
}
