<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('works_id')->constrained('works')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->string('title');
            $table->text('content');
            $table->unique(['works_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works_translations');
    }
}
