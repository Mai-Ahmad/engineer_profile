<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountersTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counters_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('counters_id')->constrained('counters')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->string('name');
            $table->unique(['counters_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counters_translations');
    }
}
