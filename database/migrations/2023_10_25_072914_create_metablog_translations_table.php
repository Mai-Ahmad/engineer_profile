<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetablogTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metablog_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('metablog_id')->constrained('metablog')->onDelete('cascade');
            $table->string('locale', 2)->index();
            $table->string('title');
            $table->text('description');
            $table->text('keywords');
            $table->unique(['metablog_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metablog_translations');
    }
}
