<?php

return [

    'home' => 'الصفحة الرئيسية',
    'about_us' => 'من نحن',
    'work_process' => 'خطوات العمل',
    'service' => 'خدماتنا',
    'our_work' => 'أعمالنا',
    'discover_more' => 'المزيد',
    'ABOUT_US' => 'من نحن',
    'WORK_PROCESS' => 'خطوات العمل',
    'WORK_PROCESS_CONT' => 'تمكين عملك باستخدام التقنيات',
    'CREATIVE_SERVICES' => 'خدماتنا الإبداعية',
    'CREATIVE_SERVICES_CONT' => 'شريكك في النجاح الرقمي',
    'OUR_WORKS' => 'أعمالنا',
    'OUR_WORKS_CONT' => 'اكتشف عالماً من البدائل المستدامة',
    'quick_links' => 'روابط سريعة',
    'certificates_and_licenses' => 'الشهادات والتراخيص',
    'all_rights_reserved' => 'كل الحقوق محفوظة',
    'view_all' => 'الكل',
    'blog' => 'المدونة',
    'recent_blogs' => 'أحدث المدونات',
    'blog_details' => 'تفاصيل المدونة',
    'Contact_Us' => 'تواصل معنا ',
    'Name' => 'أدخل اسمك',
    'Email' => 'أدخل بريدك الالكتروني',
    'Phone' => 'أدخل رقم هاتفك',
    'Message' => 'الرسالة',
    'Send' => 'إرسال',
    'inbox_email' => 'البريد الوارد',
    'name' => 'الاسم',
    'email' => 'البريد الالكتروني',
    'phone' => ' رقم الهاتف',
    'message' => 'الرسالة ',
    'sucsee' => 'تم الإرسال بنجاح . شكرا لتواصلكم معنا',
    'close' =>'موافق' ,

    


];
