<?php

return [

    'home' => 'Home',
    'about_us' => 'About Us',
    'work_process' => 'Work Process',
    'service' => 'Service',
    'our_work' => 'Our Work',
    'discover_more' => 'Discover More',
    'ABOUT_US' => 'ABOUT US',
    'WORK_PROCESS' => 'WORK PROCESS',
    'WORK_PROCESS_CONT' => 'Empowering Your Business With Artificial',
    'CREATIVE_SERVICES' => 'CREATIVE SERVICES',
    'CREATIVE_SERVICES_CONT' => 'Your Partner In Digital Success',
    'OUR_WORKS' => 'OUR WORKS',
    'OUR_WORKS_CONT' => 'Discover a World of Sustainable Alternatives',
    'quick_links' => 'Quick Links',
    'certificates_and_licenses' => 'Certificates and licenses',
    'all_rights_reserved' => 'All Rights Reserved',
    'view_all' => 'View All',
    'blog' => 'Blog',
    'recent_blogs' => 'Recent Blogs',
    'blog_details' => 'Blog Details',
    'Contact_Us' => 'Contact Us',
    'Name' => 'Your Name',
    'Email' => 'Your Email',
    'Phone' => 'Your Phone',
    'Message' => 'Your Message',
    'Send' => 'Send',
    'sucsee' => 'Sent successfully. Thank you for contacting us',
    'close' =>'OK' ,


];
