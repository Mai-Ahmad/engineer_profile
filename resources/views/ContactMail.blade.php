@php
    $lang = Illuminate\Support\Facades\Session::get('locale')
@endphp



<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
</head>
 @if($lang=='en')
<body  style="direction:ltr;padding: 50px;font-family: 'Cairo';">
@else
<body  style="direction:rtl;padding: 50px;font-family: 'Cairo';">
@endif

@if($lang=='en')
<div style="background-color:#fff;border-radius:5px;font-family: 'Cairo';
          box-shadow: rgba(0, 0, 0, 0.33) 0px 0px 10px 1px; width:auto; border:solid rgb(209, 209, 209) 1px;">
         <div style="height: 50px; background-color:#1d337f; width: 100%;color: #fff;
               padding: 10px 0px;
               border-radius: 5px;
    font-size: 20px;
    line-height: 50px;"> 
    <p style="padding-left: 40px; margin: 0;font-weight: 600;" >Inbox Email</p></div>

       
       <div style="border-radius: 30px;align-items :center; padding:20px;">

            <div style="padding: 0 10px;">
                <div style="width: 230px;
                     height: auto;display: inline-block;vertical-align: middle;">
                    <img style="width: 100%;height: 100%" src="https://www.fyanbu.com/assets/img/WhatsApp%20Image%202023-03-20%20at%2011.28.17%20AM.jpeg" alt="">
                </div>
            </div>
        </div>
            <div style="width:80%;
            padding: 10px 40px;
            border-bottom: solid 1px rgb(209, 209, 209);">
              <h3 style="font-size: 18px;font-family:'Cairo';color:#1d337f;">Name:<span style="color:#000"> {{$details['name']}} </span> </h3>
              <h3 style="font-size: 18px;font-family: 'Cairo'; color:#1d337f">  Email: <span style="color:#000">
              {{$details['email']}}</span> </h3>
            <h3 style="font-size: 18px;font-family: 'Cairo'; color:#1d337f"> Phone:<span style="color:#000"> {{$details['phone']}}</span>
            </h3>
            </div>
        <div style="
    
        width: 80%;
        padding: 10px 40px;">
            <div style="font-size: 15px; ">
                <h3 style="font-size: 18px;font-family: 'Cairo'; color:#1d337f">Message: </h3> <p  style="color:#000; font-size:18px;"> {{$details['message']}}</p>
            </div>
        </div>
    </div>

@else
<div style="background-color:#fff;border-radius:5px;font-family: 'Cairo';
          box-shadow: rgba(0, 0, 0, 0.33) 0px 0px 10px 1px; width:auto; border:solid rgb(209, 209, 209) 1px;">
         <div style="height: 50px; background-color:#1d337f; width: 100%;color: #fff;
               padding: 10px 0px;
               border-radius: 5px;
    font-size: 20px;
    line-height: 50px;"> 
    <p style="padding-right: 40px; margin: 0;font-weight: 600;" >البريد الالكتروني الوارد </p></div>

       
       <div style="border-radius: 30px;align-items :center; padding:20px;">

            <div style="padding: 0 10px;">
                <div style="width: 230px;
                     height: auto;display: inline-block;vertical-align: middle;">
                    <img style="width: 100%;height: 100%" src="https://www.fyanbu.com/assets/img/WhatsApp%20Image%202023-03-20%20at%2011.28.17%20AM.jpeg" alt="">
                </div>
            </div>
        </div>
            <div style="width:80%;
            padding: 10px 40px;
            border-bottom: solid 1px rgb(209, 209, 209);">
              <h3 style="font-size: 18px;font-family:'Cairo';color:#1d337f;">الاسم:<span style="color:#000"> {{$details['name']}} </span> </h3>
              <h3 style="font-size: 18px;font-family: 'Cairo'; color:#1d337f">  البريد الالكتروني: <span style="color:#000">
              {{$details['email']}}</span> </h3>
            <h3 style="font-size: 18px;font-family: 'Cairo'; color:#1d337f"> رقم الهاتف:<span style="color:#000"> {{$details['phone']}}</span>
            </h3>
            </div>
        <div style="
    
        width: 80%;
        padding: 10px 40px;">
            <div style="font-size: 15px; ">
                <h3 style="font-size: 18px;font-family: 'Cairo'; color:#1d337f">الرسالة: </h3> <p  style="color:#000; font-size:18px;"> {{$details['message']}}</p>
            </div>
        </div>
    </div>
    
    @endif
  
</body>

</html>