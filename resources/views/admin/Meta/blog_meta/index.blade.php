@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">اعدادات Meta لصفحة المدونة</li>
                                    <li class="breadcrumb-item active">  تعديل اعدادات Meta </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">تعديل الاعدادات Meta  للصفحة المدونة</h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">
                                <form class=" g-3" method="post" action="{{route('admin_panel.bolg_meta.update',1)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                   

                                   <div class="row">
                                       <div class="col-md-12">
                                       <div class="row">
                                       <div class="col-md-6">
                                       <label for="inputEmail4" class="form-label">الصورة</label>
                                           <input type="file" class="form-control no-border"   name="image">
                                           </div>
                                       <div class="col-md-6">
                                       <img alt="avatar" src="/{{$blog_meta->image}}"  width="300" height="200"/>
                                       </div>
                                         </div>  
                                       </div>
                                       <div class="col-md-6">
                                           <label for="inputEmail4" class="form-label">العنوان باللغة العربية</label>
                                           <input type="text" class="form-control"  value="{{$blog_meta->translate('ar')->title}}" name="title_ar">
                                       </div>
                                       <div class="col-md-6">
                                           <label for="inputEmail4" class="form-label"> العنوان باللغة الأجنبية</label>
                                           <input type="text" class="form-control"  value="{{$blog_meta->translate('en')->title}}" name="title_en">
                                       </div>
                                       <div class="col-md-12">
                                           <label for="inputEmail4" class="form-label">الوصف باللغة العربية</label>
                                           <textarea class="form-control" name="description_ar">{{$blog_meta->translate('ar')->description}}</textarea>
                                       </div>
                                       <div class="col-md-">
                                           <label for="inputEmail4" class="form-label"> الوصف باللغة الأجنبية</label>
                                           <textarea class="form-control" name="description_en">{{$blog_meta->translate('en')->description}}</textarea>
                                       </div>
                                       <div class="col-md-6">
                                           <label for="inputEmail4" class="form-label">الكلمات المفتاحية باللغة العربية</label>
                                           <input type="text" class="form-control"  value="{{$blog_meta->translate('ar')->keywords}}" name="keywords_ar">
                                       </div>
                                       <div class="col-md-6">
                                           <label for="inputEmail4" class="form-label">الكلمات المفتاحية باللغة الأجنبية</label>
                                           <input type="text" class="form-control"  value="{{$blog_meta->translate('en')->keywords}}" name="keywords_en">
                                       </div>
                                       <div class="col-12">
                                           <div class="meta-btn">
                                               <button type="submit" class="btn btn-primary">تعديل</button>
                                           </div>
                                       </div>

                                   </div>

                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
