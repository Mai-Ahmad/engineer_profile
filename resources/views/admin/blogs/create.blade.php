@extends('admin.dashboard')

@section('content')
    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item ">المدونة </li>
                                    <li class="breadcrumb-item active"> إضافة مدونة </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">إضافة مدونة </h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">
                                <form class=" g-3" method="post" action="{{route('admin_panel.blogs.store')}}" enctype="multipart/form-data" >
                                    @method('POST')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="inputAddress" class="form-label"> الصورة </label>
                                            <input type="file" class="form-control"  placeholder="1234 Main St" name="file" required>
                                        </div>

                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label">العنوان باللغة العربية </label>
                                            <input type="text" class="form-control"  name="title_ar" required>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label">المحتوى باللغة العربية </label>
                                            <textarea id="text-ar"  name="content_ar" required></textarea>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label">العنوان باللغة الأجنبية </label>
                                            <input type="text" class="form-control"  name="title_en" required>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="inputEmail4" class="form-label">المحتوى باللغة الأجنبية </label>
                                            <textarea id="text-en"   name="content_en" required></textarea>
                                        </div>

                                        <div class="col-12">
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">إضافة </button>
                                            </div>
                                        </div>

                                    </div>

                                </form>
                                <script>
                                    $(document).ready(function() {
                                        $('#text-en').summernote();
                                    });
                                    $(document).ready(function() {
                                        $('#text-ar').summernote();
                                    });

                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
