@extends('admin.dashboard')

@section('content')


    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> وسائل التواصل الاجتماعي </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class=""> التواصل الاجتماعي </h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">
                                @if(session()->has('message'))

                                    <div class="alert alert-success">

                                        {{ session()->get('message') }}

                                    </div>

                                @endif
                                <form class=" g-3" method="post" action="{{route('admin_panel.media.update',$media->id)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                  <div class="row">
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">facebook</label>
                                          <input type="url" class="form-control"  value="{{$media->facebook}}" name="facebook">
                                      </div>
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">linkedin</label>
                                          <input type="url" class="form-control"  value="{{$media->linkedin}}" name="linkedin">
                                      </div>
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">twitter</label>
                                          <input type="url" class="form-control"  value="{{$media->twitter}}" name="twitter">
                                      </div>
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">instagram</label>
                                          <input type="url" class="form-control"  value="{{$media->instagram}}" name="instagram">
                                      </div>
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">youtube</label>
                                          <input type="url" class="form-control"  value="{{$media->youtube}}" name="youtube">
                                      </div>
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">snapchat</label>
                                          <input type="url" class="form-control"  value="{{$media->snapchat}}" name="snapchat">
                                      </div>
                                      <div class="col-md-6">
                                          <label for="inputEmail4" class="form-label">whatsapp</label>
                                          <input type="tel" class="form-control"  value="{{$media->whatsapp}}" name="whatsapp">
                                      </div>


                                      <div class="col-md-12">
                                          <div class="">
                                              <button type="submit" class="btn btn-primary">تعديل</button>
                                          </div>
                                      </div>
                                  </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>


@endsection
