@extends('admin.dashboard')

@section('content')
    <head>
{{--        <style>--}}
{{--            td {--}}
{{--                white-space: normal;--}}
{{--            }--}}
{{--        </style>--}}
    </head>
    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> خطوات المشروع </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="col-12" style="margin:2% 2% auto;">
            <div class="user-profile ">
                <div class="widget-content widget-content-area">

                    <div class="" style="padding: 2%;">
                        <div class="container">
                            @if(session()->has('message'))

                                <div class="alert alert-success">

                                    {{ session()->get('message') }}

                                </div>

                            @endif
                            <div class="table-responsive" id="t1">
                                <table id="myTable1" class="table table-striped table-bordered table-sm">
                                    <thead>
                                    <tr>
                                        <th class="text-center" scope="col"></th>
                                        <th scope="col">الأيقونة </th>
                                        <th scope="col">العنوان باللغة العربية </th>
                                        <th scope="col">المحتوى باللغة العربية </th>
                                        <th scope="col">العنوان باللغة الأجنبية </th>
                                        <th scope="col">المحتوى باللغة الأجنبية </th>

                                        <th class="text-center" scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $counter=1;?>
                                    @foreach($items as $item)

                                        <tr>
                                            <td>
                                                <p class="text-center">{{$counter}}</p>
                                                <span class="text-success"></span>
                                                <?php $counter++;?>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="avatar me-2">
                                                        <img alt="avatar" src="/{{$item->icon}}" class="rounded-circle" />
                                                    </div>

                                                </div>
                                            </td>
                                            <td>
                                                <p class="mb-0">{{$item->translate('ar')->title}}</p>
                                                <span class="text-success"></span>
                                            </td>
                                           <td>
                                                <p class="mb-0">{{$item->translate('ar')->content}}</p>
                                                <span class="text-success"></span>
                                            </td>

                                            <td>
                                                <p class="mb-0">{{$item->translate('en')->title}}</p>
                                                <span class="text-success"></span>
                                            </td>
                                            <td>
                                                <p class="mb-0">{{$item->translate('en')->content}}</p>
                                                <span class="text-success"></span>
                                            </td>

                                            <td class="text-center">
                                                <div class="action-btns">
                                                    <a href="{{route('admin_panel.yourProject.edit',$item->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="تعديل">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                    </a>

                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>

                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->



@endsection
