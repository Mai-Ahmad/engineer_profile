@php
    $lang = App::getLocale()
@endphp
    <!DOCTYPE html>
<html lang="zxx">

<head>
@if($lang=='ar')
<meta name="title" content="{{$blog->translate($lang)->title}}">
<meta property="og:title" content="{{$blog->translate($lang)->title}}}">
<meta name="description" content="{!! substr(strip_tags($blog->translate($lang)->content), 0, 200) !!}">
<meta property="og:description" content="{!! substr(strip_tags($blog->translate($lang)->content), 0, 200) !!}"> 
<meta name="keywords" content="{{$blog_meta->translate('ar')->keywords}}">
@else
<meta name="title" content="{{$blog->translate($lang)->title}}">
<meta property="og:title" content="{{$blog->translate($lang)->title}}">
<meta name="description" content="{!! substr(strip_tags($blog->translate($lang)->content), 0, 200) !!}">
<meta property="og:description" content="{!! substr(strip_tags($blog->translate($lang)->content), 0, 200) !!}"> 
<meta name="keywords" content="{{$blog_meta->translate('en')->keywords}}">
@endif
<meta property="og:type" content="website">
<meta property="image" content="/{{$blog->image}}">
<meta property="og:image" content="/{{$blog->image}}">
<meta property="og:image:height" content="300">
<meta property="og:image:width" content="300">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">





    
    <link rel=icon href="{{asset('assets/img/favicon.png')}}" sizes="20x20" type="image/png">

    <!-- Stylesheet -->
    @if($lang=='ar')
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-rtl.min.css')}}">
    @else
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    @endif
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.min.css')}}">
    @if($lang=='ar')
    <link rel="stylesheet" href="{{asset('assets/css/style-rtl.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive-rtl.css')}}">
    @else
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    @endif

</head>

<body>

<!-- search popup start-->
<div class="td-search-popup" id="td-search-popup">
    <form action="index.html" class="search-form">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search.....">
        </div>
        <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
    </form>
</div>
<!-- search popup end-->
<div class="body-overlay" id="body-overlay"></div>
<!-- navbar start -->
<nav class="navbar navbar-area navbar-expand-lg">
    <div class="container nav-container navbar-bg">
        <div class="responsive-mobile-menu">
            <button class="menu toggle-btn d-block d-lg-none" data-target="#itech_main_menu" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="icon-left"></span>
                <span class="icon-right"></span>
            </button>
        </div>
        <div class="logo">
            <a href="{{URL::to('/').'/'.$lang}}"><img src="{{asset('assets/img/WhatsApp Image 2023-03-20 at 11.28.17 AM.jpeg')}}" alt="img"></a>
        </div>
        <div class="nav-right-part nav-right-part-mobile">
            <a class="search-bar-btn" href="#">
                <i class="fa fa-search"></i>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="itech_main_menu">
            <ul class="navbar-nav menu-open ">
                <li><a href="{{URL::to('/').'/'.$lang}}">{{trans('home_page.home')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#about-us">{{trans('home_page.about_us')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#Work-Process">{{trans('home_page.work_process')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#Service">{{trans('home_page.service')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#Our-Work">{{trans('home_page.our_work')}}</a></li>
                <li><a class="active" href="{{route('home_blogs.index')}}">{{trans('home_page.blog')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#contact">{{trans('home_page.Contact_Us')}}</a></li>

                {{--                <li><a href="/home_blogs/{{$lang}}">{{trans('home_page.blog')}}</a></li>--}}

                @if($lang=='en')
                    <li>
                        <a href="{{ LaravelLocalization::getLocalizedURL('ar') }}">العربية</a>
                    </li>
                @else
                    <li>
                        <a  href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>
<!-- navbar end -->

<!-- page title start -->
<div class="breadcrumb-area bg-cover" style="background-image: url('./assets/img/bg/7.png');">
    <div class="container">
        <div class="breadcrumb-inner">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <h2 class="page-title">{{trans('home_page.blog_details')}}</h2>
                </div>
                <div class="col-lg-6 text-lg-end">
                    <ul class="page-list">
                        <li><a href="{{URL::to('/').'/'.$lang}}">{{trans('home_page.blog')}}</a></li>
                        <li>{{trans('home_page.blog_details')}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- page title end -->

<!-- blogs area start -->
<div class="blog-area pd-bottom-120">
    <div class="container">
        <div class="row">
             
            <div class="col-lg-8 col-md-12">

                <div class="blog-details-page-content">
                    <div class="single-blog-inner">
                              <div class="thumb">
                                <img src="/{{$blog->image}}" alt="img">
                            </div>
                            <div class="details">
                                <p class="date mb-3"><i class="far fa-calendar-alt"></i>{{$blog->created_at->toDateString()}}</p>
                                <h5>{{$blog->translate($lang)->title}}</h5>
                                <div class="meta">
                                    <p>{!!$blog->translate($lang)->content!!}</p>
                                </div>
                    </div>
                </div>
            </div>
            </div>






            <div class="col-lg-4 col-md-12">
                <div class="td-sidebar">
                    <div class="widget widget-recent-post">
                        <h4 class="widget-title">{{trans('home_page.recent_blogs')}}</h4>
                        <ul>
                            <div class="row">
                               @foreach($last_blogs as $last)
                                    <div class="col-lg-12 col-md-6">
                                        <li>
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="/{{$last->image}}" alt="img">
                                                </div>
                                                <div class="media-body align-self-center">
                                                    <h6 class="title"><a href="{{route('home_blogs.show',$blog->id)}}">{{$last->translate($lang)->title}}</a></h6>
                                                    <div class="post-info"><i class="far fa-calendar-alt"></i><span>{{$last->created_at->toDateString()}}</span></div>
                                                </div>
                                            </div>
                                        </li>
                                    </div>
                                @endforeach

                            </div>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- footer area start -->
<footer class="footer-area footer-area-2 mt-0 pd-top-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="widget widget_about">
                    <div class="thumb">
                       <a href="{{URL::to('/').'/'.$lang}}"><img src="{{asset('assets/img/logologo.jpeg')}}" alt="img"></a> 
                    </div>
                    <div class="details">
                        <p>حاصل على رخصة عمل حر ومتواجد في مدينة الرياض</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 padd">
                <div class="widget widget_nav_menu">
                    <h4 class="widget-title">{{trans('home_page.quick_links')}} </h4>
                    <ul>
                        <li><a href="{{URL::to('/').'/'.$lang}}"><i class="fas fa-chevron-right"></i>{{trans('home_page.home')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#about-us"><i class="fas fa-chevron-right"></i>{{trans('home_page.about_us')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#Work-Process"><i class="fas fa-chevron-right"></i>{{trans('home_page.work_process')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#Service"><i class="fas fa-chevron-right"></i>{{trans('home_page.service')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#Our-Work"><i class="fas fa-chevron-right"></i>{{trans('home_page.our_work')}}</a></li>
                        <li><a href="{{route('home_blogs.index')}}"><i class="fas fa-chevron-right"></i>{{trans('home_page.blog')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#contact"><i class="fas fa-chevron-right"></i>{{trans('home_page.Contact_Us')}}</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 ps-xl-5">
                <div class="widget widget_nav_menu">
                    <h4 class="widget-title">{{trans('home_page.certificates_and_licenses')}}</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="image">
                                <img src="{{asset('assets/img/freelancer1.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="image">
                                <img src="{{asset('assets/img/freelancer3.png')}}" alt="" >
                            </div>
                        </div>
                    </div>
                    <div class="widget widget_contact">
                        <ul class="social-media mt-4">
                            <li>
                                <a href="{{$media->facebook}}"  target="blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->snapchat}}" target="blank">
                                    <i class="fab fa-snapchat-ghost"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->linkedin}}" target="blank">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->twitter}}" target="blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->instagram}}" target="blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->youtube}}" target="blank">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 align-self-center white-p">
                    <p class="white-p">{{trans('home_page.all_rights_reserved')}} </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->










