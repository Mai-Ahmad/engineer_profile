@php
    $lang = App::getLocale()
@endphp

<!DOCTYPE html>
<html lang="zxx">

<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P3BB9CZ6');</script>
<!-- End Google Tag Manager -->


    <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BDS0VZYEB9"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-BDS0VZYEB9');
</script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-G58DVJHV7N"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-G58DVJHV7N');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-KCV9EY6G4K"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-KCV9EY6G4K');
</script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-11204309403"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-11204309403');
</script>



@if($lang=='ar')
<meta name="title" content="{{$meta->translate('ar')->title}}">
<meta property="og:title" content="{{$meta->translate('ar')->title}}">
<meta name="description" content="{{$meta->translate('ar')->description}}">
<meta property="og:description" content="{{$meta->translate('ar')->description}}"> 
<meta name="keywords" content="{{$meta->translate('ar')->keywords}}">
   @else
   <meta name="title" content="{{$meta->translate('en')->title}}">
<meta property="og:title" content="{{$meta->translate('en')->title}}">
<meta name="description" content="{{$meta->translate('en')->description}}">
<meta property="og:description" content="{{$meta->translate('en')->description}}"> 
<meta name="keywords" content="{{$meta->translate('en')->keywords}}">
    @endif




<meta property="og:type" content="website">
<meta property="image" content="/{{$meta->image}}">
<meta property="og:image" content="/{{$meta->image}}">
<meta property="og:image:height" content="300">
<meta property="og:image:width" content="300">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">

    
    <!-- Stylesheet -->
    @if($lang=='ar')
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-rtl.min.css')}}">
    @else
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    @endif
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.min.css')}}">
    @if($lang=='ar')
    <link rel="stylesheet" href="{{asset('assets/css/style-rtl.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive-rtl.css')}}">
    @else
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    @endif

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3BB9CZ6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<div class="body-overlay" id="body-overlay"></div>
<!-- navbar start -->
<nav class="navbar navbar-area navbar-expand-lg">
    <div class="container nav-container navbar-bg">
        <div class="responsive-mobile-menu">
            <button class="menu toggle-btn d-block d-lg-none" data-target="#itech_main_menu" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="icon-left"></span>
                <span class="icon-right"></span>
            </button>
        </div>
        <div class="logo">
            <a href="{{URL::to('/').'/'.$lang}}"><img src="{{asset('assets/img/WhatsApp Image 2023-03-20 at 11.28.17 AM.jpeg')}}" alt="img"></a>
        </div>
         
        <div class="collapse navbar-collapse" id="itech_main_menu">
            <ul class="navbar-nav menu-open ">
                <li><a href="{{URL::to('/').'/'.$lang}}">{{trans('home_page.home')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#about-us">{{trans('home_page.about_us')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#Work-Process">{{trans('home_page.work_process')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#Service">{{trans('home_page.service')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#Our-Work">{{trans('home_page.our_work')}}</a></li>
                <li><a href="{{route('home_blogs.index')}}">{{trans('home_page.blog')}}</a></li>
                <li><a href="{{URL::to('/').'/'.$lang}}#contact">{{trans('home_page.Contact_Us')}}</a></li>

                @if($lang=='en')
                    <li>
                        <a class="lang" href="{{ LaravelLocalization::getLocalizedURL('ar') }}">العربية</a>
                    </li>

                @else
                    <li>
                        <a  class="lang" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
                    </li>
                @endif

            </ul>
        </div>

    </div>
</nav>

<!-- navbar end -->

<!-- page title start -->

<div class="banner-area bg-relative banner-area-2 bg-cover"
     style="background-image: url('./assets/img/banner-3/5.png');" id="home">
    <img class="bg-img-2" src="{{asset('assets/img/banner-3/4.png')}}" alt="img">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-center">
                <div class="banner-inner pe-xl-5">
                    <h2 class="title wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.4s">
                        {{$about->translate($lang)->title}}


                    </h2>
                    <p class="content pe-xl-5 wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        {{$about->translate($lang)->content}}
                    </p>
                    <a class="btn btn-border-base wow animated fadeInLeft" data-wow-duration="1.5s"
                       data-wow-delay="0.6s" href="#about-us">{{trans('home_page.discover_more')}} <i class="fa fa-plus"></i></a>
                    <div class="d-inline-block align-self-center wow animated fadeInLeft" data-wow-duration="1.5s"
                         data-wow-delay="0.7s">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-10">
                <div class="banner-thumb-3 wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.3s">
                    <div class="main-img-wrap">
                        <img class="main-img" src="{{asset('assets/img/banner-3/1.png')}}" alt="img">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner end -->

<!-- about area start -->
<div class="about-area pd-top-120" id="about-us">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about-thumb-inner pe-xl-5 me-xl-5 wow animated fadeInLeft" data-wow-duration="1.5s"
                     data-wow-delay="0.3s">
                    <img class="animate-img-1 top_image_bounce" src="{{asset('assets/img/about/2.png')}}" alt="img">
                    <img class="animate-img-2 left_image_bounce" src="{{asset('assets/img/about/3.png')}}" alt="img">
                    <img class="animate-img-3 top_image_bounce" src="{{asset('assets/img/banner/5.svg')}}" alt="img">

                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.3s">
                <div class="section-title mt-5 mt-lg-0">
                    <h6 class="sub-title">{{trans('home_page.ABOUT_US')}}</h6>
                    <h2 class="title">{{$who_are_we->translate($lang)->title }}</h2>
                    <p class="content mb-4 mb-xl-5">
                        {{$who_are_we->translate($lang)->content }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about area end -->

<!-- work-process-area start -->
<div class="work-process-area bg-position-right pd-top-120 pd-bottom-90"
     style="background-image: url('./assets/img/bg/16.png');" id="Work-Process">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center pb-5">
                    <h6 class="sub-title">{{trans('home_page.WORK_PROCESS')}}</h6>
                    <h2 class="title">{{trans('home_page.WORK_PROCESS_CONT')}} </h2>
                </div>
            </div>
        </div>
        <div class="work-process-area-inner-2">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-work-process-inner style-2 text-center">
                        <img class="line-img" src="{{asset('assets/img/about/29.png')}}" alt="img">
                        <div class="thumb mb-3">
                            <img src="{{$step1->icon}}" alt="img">
                        </div>
                        <div class="details">
                            <h5 class="mb-3">{{$step1->translate($lang)->title}}</h5>
                            <p class="content">{{$step1->translate($lang)->content}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-work-process-inner style-2 text-center">
                        <img class="line-img" src="{{asset('assets/img/about/30.png')}}" alt="img">
                        <div class="thumb mb-3">
                            <img src="{{$step2->icon}}" alt="img">
                        </div>
                        <div class="details">
                            <h5 class="mb-3">{{$step2->translate($lang)->title}}</h5>
                            <p class="content">{{$step2->translate($lang)->content}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-work-process-inner style-2 text-center">
                        <img class="line-img" src="{{asset('assets/img/about/29.png')}}" alt="img">
                        <div class="thumb mb-3">
                            <img src="{{$step3->icon}}" alt="img">
                        </div>
                        <div class="details">
                            <h5 class="mb-3">{{$step3->translate($lang)->title}}</h5>
                            <p class="content">{{$step3->translate($lang)->content}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-work-process-inner style-2 text-center">
                        <div class="thumb mb-3">
                            <img src="{{$step4->icon}}" alt="img">
                        </div>
                        <div class="details">
                            <h5 class="mb-3">{{$step4->translate($lang)->title}}</h5>
                            <p class="content">{{$step4->translate($lang)->content}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- work-process-area end -->

<!-- service area start -->
<div class="service-area bg-relative pd-top-100" id="Service">
    <img class="position-bottom-left top_image_bounce" src="{{asset('assets/img/icon/4.png')}}" alt="img">
    <div class="container">
        <div class="section-title text-center">
            <h6 class="sub-title">{{trans('home_page.CREATIVE_SERVICES')}}</h6>
            <h2 class="title">{{trans('home_page.CREATIVE_SERVICES_CONT')}}</h2>
        </div>
        <div class="row">
            @foreach($services as $service)
            <div class="col-lg-4 col-md-6">
                <div class="single-service-inner text-center">
                    <div class="thumb">
                        <img src="/{{$service->icon}}" alt="img">
                    </div>
                    <div class="details">
                        <h5>{{$service->translate($lang)->title}}</h5>
                        <p>{{$service->translate($lang)->content}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- service area end -->

<!-- project area start -->
<div class="project-section pd-top-120" id="Our-Work">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center">
                    <h6 class="sub-title">{{trans('home_page.OUR_WORKS')}}</h6>
                    <h2 class="title">{{trans('home_page.OUR_WORKS_CONT')}}</h2>
                </div>
            </div>
        </div>
        <nav class="text-center">
            <div class="nav nav-tabs project-nav-tab d-inline-flex mb-5" id="nav-tab" role="tablist">
                <button class="nav-link active" id="nav-tabs1-tab" data-bs-toggle="tab" data-bs-target="#nav-tabs1"
                        type="button" role="tab" aria-controls="nav-tabs1" aria-selected="true">{{trans('home_page.view_all')}}</button>
                <button class="nav-link" id="nav-tabs2-tab" data-bs-toggle="tab" data-bs-target="#nav-tabs2"
                        type="button" role="tab" aria-controls="nav-tabs2" aria-selected="false">{{$type1->translate($lang)->name}}</button>
                <button class="nav-link" id="nav-tabs3-tab" data-bs-toggle="tab" data-bs-target="#nav-tabs3"
                        type="button" role="tab" aria-controls="nav-tabs3" aria-selected="false">{{$type2->translate($lang)->name}}</button>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-tabs1" role="tabpanel" aria-labelledby="nav-tabs1-tab">
                <div class="row">
                    @foreach($works as $work)
                     <div class="col-lg-4 col-md-6">
                        <div class="single-blog-list style-3">
                            <div class="thumb">
                                <img src="{{$work->image}}" alt="img">
                            </div>
                            <div class="details">
                                <ul class="blog-meta">
                                    <li>{{$work->translate($lang)->title}}</li></a>
                                </ul>
                                <h5 class="mb-3"><a href="blog-details.html">{{$work->translate($lang)->title}}</a></h5>
                                <p>{{$work->translate($lang)->content}}</p>
                                <div class="btn-wrap text-end pe-2">
                                    <a class="read-more-text" href="{{$work->link}}">{{$work->link}}<i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="tab-pane fade" id="nav-tabs2" role="tabpanel" aria-labelledby="nav-tabs2-tab">
                <div class="row">
                    @foreach($works as $work)
                        @if($work->type_id==$type1->id)
                        <div class="col-lg-4 col-md-6">
                            <div class="single-blog-list style-3">
                                <div class="thumb">
                                    {{--                                <img src="{{asset('assets/img/blogs/4.png')}}" alt="img">--}}
                                    <img src="{{$work->image}}" alt="img">
                                </div>
                                <div class="details">
                                    <ul class="blog-meta">
                                        <li>{{$work->translate($lang)->title}}</li></a>
                                    </ul>
                                    <h5 class="mb-3"><a href="blog-details.html">{{$work->translate($lang)->title}}</a></h5>
                                    <p>{{$work->translate($lang)->content}}</p>
                                    <div class="btn-wrap text-end pe-2">
                                        <a class="read-more-text" href="{{$work->link}}">{{$work->link}}<i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach

                </div>
            </div>
            <div class="tab-pane fade" id="nav-tabs3" role="tabpanel" aria-labelledby="nav-tabs3-tab">
                <div class="row">
                    @foreach($works as $work)
                        @if($work->type_id==$type2->id)
                        <div class="col-lg-4 col-md-6">
                            <div class="single-blog-list style-3">
                                <div class="thumb">
                                    {{--                                <img src="{{asset('assets/img/blogs/4.png')}}" alt="img">--}}
                                    <img src="{{$work->image}}" alt="img">
                                </div>
                                <div class="details">
                                    <ul class="blog-meta">
                                        <li>{{$work->translate($lang)->title}}</li></a>
                                    </ul>
                                    <h5 class="mb-3"><a href="blog-details.html">{{$work->translate($lang)->title}}</a></h5>
                                    <p>{{$work->translate($lang)->content}}</p>
                                    <div class="btn-wrap text-end pe-2">
                                        <a class="read-more-text" href="{{$work->link}}">{{$work->link}}<i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
<!-- project area start -->
        <!-- contact area start -->
<!-- Button trigger modal -->


        <div class="contact-area pd-top-120 pd-bottom-120" id="contact">
            <div class="container">
                <div class="contact-page-inner bg-gray">
                    <div class="section-title mb-4 pb-4">
                        <h2 class="title">{{trans('home_page.Contact_Us')}}</h2>
                    </div>
               

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
     <p> {{trans('home_page.sucsee')}}<p>
      </div>
      <div class="modal-footer">
      <button type="button"  id="ok" class="btn btn-base  border-radius-5" data-bs-dismiss="modal">{{trans('home_page.close')}}</button>

      </div>
    </div>
  </div>
</div>

 <form id="contactForm" method="post" action="/contact"> 
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="single-input-inner">
                  <input id="name" name="name" type="text" placeholder="{{trans('home_page.Name')}}">
             </div>
         </div>
         <div class="col-md-6">
             <div class="single-input-inner">
             <input id="email" name="email" type="email" placeholder="{{trans('home_page.Email')}}">
             </div>
         </div>
         <div class="col-md-12">
             <div class="single-input-inner">
                  <input id="phone" name="phone" type="number" placeholder="{{trans('home_page.Phone')}}">
            </div>
         </div>
          <div class="col-md-12">
             <div class="single-input-inner">
             <textarea name="message" id="message" placeholder="{{trans('home_page.Message')}}"></textarea>
             </div>
         </div>
                
         <div class="col-12 text-center">
              <button id="submit" type="submit" class="btn btn-base border-radius-5">{{trans('home_page.Send')}}</button>
        </div>
    </div>
 </div>
</form>

                </div>
            </div>
        </div>
        <!-- contact area end -->



<!-- footer area start -->
<footer class="footer-area footer-area-2 mt-0 pd-top-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="widget widget_about">
                <div class="thumb">
                       <a href="{{URL::to('/').'/'.$lang}}"><img src="{{asset('assets/img/logologo.jpeg')}}" alt="img"></a> 
                    </div>
                    <div class="details">
                        <p>موقعنا : الرياض - حي السليمانية - شارع ابن كثير</p>
                   
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 padd">
                <div class="widget widget_nav_menu">
                    <h4 class="widget-title">{{trans('home_page.quick_links')}} </h4>
                    <ul>
                        <li><a href="#home"><i class="fas fa-chevron-right"></i>{{trans('home_page.home')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#about-us"><i class="fas fa-chevron-right"></i>{{trans('home_page.about_us')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#Work-Process"><i class="fas fa-chevron-right"></i>{{trans('home_page.work_process')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#Service"><i class="fas fa-chevron-right"></i>{{trans('home_page.service')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#Our-Work"><i class="fas fa-chevron-right"></i>{{trans('home_page.our_work')}}</a></li>
                        <li><a href="{{route('home_blogs.index')}}"><i class="fas fa-chevron-right"></i>{{trans('home_page.blog')}}</a></li>
                        <li><a href="{{URL::to('/').'/'.$lang}}#contact"><i class="fas fa-chevron-right"></i>{{trans('home_page.Contact_Us')}}</a></li>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 ps-xl-5">
                <div class="widget widget_nav_menu">
                    <h4 class="widget-title">{{trans('home_page.certificates_and_licenses')}}</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="image">
                                <img src="{{asset('assets/img/freelancer1.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="image">
                                <img src="{{asset('assets/img/freelancer3.png')}}" alt="" >
                            </div>
                        </div>
                    </div>
                    <div class="widget widget_contact">
                        <ul class="social-media mt-4">
                            <li>
                                <a href="{{$media->facebook}}" target="blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->snapchat}}" target="blank">
                                    <i class="fab fa-snapchat-ghost"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->linkedin}}" target="blank">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->twitter}}" target="blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->instagram}}" target="blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$media->youtube}}" target="blank">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 align-self-center white-p">
                    <p class="white-p">{{trans('home_page.all_rights_reserved')}} </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->

<!-- back to top area start -->
<div class="whatsapp-icon" >
    <a href="https://wa.me/{{$media->whatsapp}}" target="blank">
    <i class="fab fa-whatsapp"></i>
    </a>
</div>
<!-- back to top area end -->


<!-- all plugins here -->




<script src="{{asset('assets/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/magnific.min.js')}}"></script>
<script src="{{asset('assets/js/nice-select.min.js')}}"></script>
<script src="{{asset('assets/js/slick.min.js')}}"></script>
<script src="{{asset('assets/js/owl.min.js')}}"></script>
<script src="{{asset('assets/js/counter-up.min.js')}}"></script>
<script src="{{asset('assets/js/waypoint.min.js')}}"></script>

<script src="{{asset('assets/js/wow.min.js')}}"></script>

<!-- main js  -->
<script src="{{asset('assets/js/main.js')}}"></script>


<!-- ajax code -->
<script>
    $(document).ready(function () {

$('#contactForm').on('submit',function (e) {
               e.preventDefault();

               var name = $('#name').val();
               var email = $('#email').val();
               var phone = $('#phone').val();
               var message = $('#message').val();

               $.ajax({
                  type: "POST",
                  url: "/contact",
                   data: $('#contactForm').serialize(),
                   success: function( responce ) {
                    console.log('responce');
                    $('#exampleModal').modal('show');
                 
                   },

               });
           });

           $('#ok').on('click' , function(){
            document.getElementById("contactForm").reset();})
       });

    

</script>


    </body>

</html>
