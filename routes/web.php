<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CountersController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\MetablogController;
use App\Http\Controllers\Admin\MetaController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\WorksController;
use App\Http\Controllers\Admin\YourProjectController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'home']);

Route::post('/contact', [ContactController::class, 'store']);

Route::get('/route-cache', function() {
     \Artisan::call('route:clear');
     return 'Routes cache cleared';
 });

 //Clear config cache
 Route::get('/config-cache', function() {
     \Artisan::call('config:cache');
     return 'Config cache cleared';
 }); 
  //Clear config cache
 Route::get('/config-clear', function() {
     \Artisan::call('config:clear');
     return 'Config cache cleared';
 });

 // Clear application cache
 Route::get('/clear-cache', function() {
     \Artisan::call('cache:clear');
     return 'Application cache cleared';
 });


//Route::get('lang/{lang}', [HomeController::class, 'switchLang'])->name('lang.switch');
//Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'HomeController@switchLang']);
Route::group(['prefix' => 'admin_panel','as' => 'admin_panel.'], function () {
    Route::get('/login_view', [AuthController::class, 'loginView'])->name('login');
    Route::post('/login', [AuthController::class, 'login']);

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', [AuthController::class, 'home']);
        Route::get('/editAbout', [SettingController::class, 'editAbout']);
        Route::post('/updateAbout', [SettingController::class, 'updateAbout']);
        Route::get('/editWe', [SettingController::class, 'editWe']);
        Route::post('/updateWe', [SettingController::class, 'updateWe']);
        Route::resource('yourProject', YourProjectController::class);
        Route::resource('services', ServicesController::class);
        Route::resource('contact', ContactController::class);
        Route::get('/delContact/{id}', [ContactController::class, 'destroy']);
        Route::get('/delService/{id}', [ServicesController::class, 'destroy']);
        Route::resource('works', WorksController::class);
        Route::get('/delWork/{id}', [WorksController::class, 'destroy']);
        Route::resource('counters', CountersController::class);
        Route::resource('media', MediaController::class);
        Route::resource('blogs', BlogController::class);
        Route::get('/delBlog/{id}', [BlogController::class, 'destroy']);
      
        Route::resource('meta', MetaController::class);
        Route::resource('bolg_meta', MetablogController::class);



        


        Route::get('/logout', [AuthController::class, 'logout']);

    });
});




Route::group(['middleware'=>['localizationRedirect', 'localeViewPath' ,'localeCookieRedirect','localeSessionRedirect']
,'prefix' => LaravelLocalization::setLocale()],function ()
{
    Route::group(['middleware'=>'language'],function ()
    {
        Route::get('/', [HomeController::class, 'index']);
        Route::resource('home_blogs', \App\Http\Controllers\BlogController::class);
    });
});






